package main

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

type Installments map[string]Installment
type Installment struct {
	Repository string
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/installments", getInstallments).Methods("GET")
	router.HandleFunc("/installments", updateInstallments).Methods("PUT")
	http.ListenAndServe(":8000", router)
}

func getInstallments(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	is := Installments{
		"foo": {Repository: "bar"},
	}
	json.NewEncoder(w).Encode(is)
}

func updateInstallments(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Write([]byte("success"))
}
